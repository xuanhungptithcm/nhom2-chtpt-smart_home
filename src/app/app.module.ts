import { RequestpasswordComponent } from "./authentication/requestpassword/requestpassword.component";
import { ActiveComponent } from "./authentication/active/active.component";
import { RegisterComponent } from "./authentication/register/register.component";
import { ReactiveFormsModule } from "@angular/forms";
import { FormsModule } from "./pages/forms/forms.module";
/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { BrowserModule } from "@angular/platform-browser";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { NgModule } from "@angular/core";
import { HttpClientModule, HTTP_INTERCEPTORS } from "@angular/common/http";
import { CoreModule } from "./@core/core.module";
import { ThemeModule } from "./@theme/theme.module";
import { AppComponent } from "./app.component";
import { AppRoutingModule } from "./app-routing.module";
import {
  NbChatModule,
  NbDatepickerModule,
  NbDialogModule,
  NbMenuModule,
  NbSidebarModule,
  NbToastrModule,
  NbWindowModule,
  NbSpinnerModule,
  NbButtonModule,
  NbInputModule,
} from "@nebular/theme";
import { SocketIoModule, SocketIoConfig } from "ngx-socket-io";
import { NbAuthModule, NbPasswordAuthStrategy } from "@nebular/auth";
import { AuthInterceptors } from "./@core/interceptors/auth.interceptor";
import { LoginComponent } from "./authentication/login/login.component";
const config: SocketIoConfig = {
  url: "http://ec2-13-250-38-29.ap-southeast-1.compute.amazonaws.com:8888",
  options: {},
};

@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    RegisterComponent,
    ActiveComponent,
    RequestpasswordComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    SocketIoModule.forRoot(config),
    ThemeModule.forRoot(),
    FormsModule,
    NbSidebarModule.forRoot(),
    NbMenuModule.forRoot(),
    NbDatepickerModule.forRoot(),
    NbDialogModule.forRoot(),
    NbWindowModule.forRoot(),
    NbToastrModule.forRoot(),
    NbChatModule.forRoot({
      messageGoogleMapKey: "AIzaSyA_wNuCzia92MAmdLRzmqitRGvCF7wCZPY",
    }),
    CoreModule.forRoot(),
    NbSpinnerModule,
    ReactiveFormsModule,
    NbInputModule,
    NbButtonModule,
  ],
  bootstrap: [AppComponent],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: AuthInterceptors,
      multi: true,
    },
  ],
})
export class AppModule {}
