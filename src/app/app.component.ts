/**
 * @license
 * Copyright Akveo. All Rights Reserved.
 * Licensed under the MIT License. See License.txt in the project root for license information.
 */
import { Component, OnInit } from "@angular/core";
import { AnalyticsService } from "./@core/utils/analytics.service";
import { SocketService } from "./@core/services/Socket.service";

@Component({
  selector: "ngx-app",
  template: "<router-outlet></router-outlet>",
})
export class AppComponent implements OnInit {
  constructor(
    private analytics: AnalyticsService,
    private socketService: SocketService
  ) {}

  ngOnInit(): void {
    this.analytics.trackPageViews();

    // this.socketService.getMessages('light').subscribe((data: any) => {
    //   console.log(data);
    //   if (data['status'] === 'true') {
    //   } else {
    //   }
    // });
    // this.socketService.getMessages('login-thanh-cong').subscribe((data: any) => {
    //   // this.messages.push(message);
    //   console.log(data);

    // });
  }
}
