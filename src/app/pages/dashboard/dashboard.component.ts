import { DeviceService } from "./../../@core/services/device.service";
import { Component, OnDestroy, OnInit, ChangeDetectorRef } from "@angular/core";
import { NbThemeService } from "@nebular/theme";
import { takeWhile } from "rxjs/operators";
import { SolarData } from "../../@core/data/solar";
import { SocketService } from "../../@core/services/Socket.service";
import { FormControl } from "@angular/forms";
import { Temperature } from "../../@core/data/temperature-humidity";
import { TemperatureHumidityService } from "../../@core/mock/temperature-humidity.service";
import { forkJoin } from "rxjs";
let uniqueId = 0;
interface CardSettings {
  title: string;
  iconClass: string;
  type: string;
  numberDevice: number;
  name: string;
  nameClass: string;
  subClass?: string;
}

@Component({
  selector: "ngx-dashboard",
  styleUrls: ["./dashboard.component.scss"],
  templateUrl: "./dashboard.component.html",
})
export class DashboardComponent implements OnDestroy, OnInit {
  private alive = true;

  solarValue: number;
  listDevice = [];

  styles = {
    viewBox: "0 0 300 300",
    arcTranslateStr: "translate(0, 0)",
    clipPathStr: "",
    gradArcs: [],
    nonSelectedArc: {},
    thumbPosition: { x: 0, y: 0 },
    blurRadius: 15,
  };
  pinRadius = 10;
  off = false;
  scaleFactor = 1;
  svgControlId = uniqueId++;
  toggleFormControl = new FormControl(false);
  temperatureData: Temperature;
  temperature: number;
  temperatureOff = false;
  temperatureMode = "cool";
  theme: any;
  themeSubscription: any;
  humidityData: Temperature;
  humidity: number;

  modeAuto: boolean = false;

  constructor(
    private themeService: NbThemeService,
    private solarService: SolarData,
    private socketService: SocketService,
    private cd: ChangeDetectorRef,
    private deviceService: DeviceService,
    private temperatureHumidityService: TemperatureHumidityService
  ) {
    this.solarService
      .getSolarData()
      .pipe(takeWhile(() => this.alive))
      .subscribe((data) => {
        this.solarValue = data;
      });
    this.themeService
      .getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe((config) => {
        this.theme = config.variables.temperature;
      });

    forkJoin(
      this.temperatureHumidityService.getTemperatureData(),
      this.temperatureHumidityService.getHumidityData()
    ).subscribe(
      ([temperatureData, humidityData]: [Temperature, Temperature]) => {
        this.temperatureData = temperatureData;
        this.temperature = this.temperatureData.value;
        this.humidityData = humidityData;
        this.humidity = this.humidityData.value;
      }
    );
  }

  ngOnDestroy() {
    this.alive = false;
  }
  ngOnInit() {
    console.log(12321);
    this.deviceService.getDevice().subscribe((data) => {
      if (data["listDevice"]) {
        this.listDevice = data["listDevice"];
      }
    });
    this.socketService.sendMessage("device", { typeDevice: "ui" });
    this.deviceService.getHumiNow().subscribe((data) => {
      console.log(data);
    });
    this.deviceService.getTempNow().subscribe((data) => {
      console.log(data);
    });
  }
  getPower2(value) {
    if (!value) {
      // this.sendResquestWithSpeed(0);
    }
  }
  getNumberSpeed(value: number) {
    if (!this.modeAuto) {
      // this.sendResquestWithSpeed(value);
      this.deviceService.setTemp(value).subscribe((data) => {
        console.log(data);
      });
    }
  }
  getNumberLight(value: number) {
    if (!this.modeAuto) {
      this.deviceService.setHumi(value).subscribe((data) => {
        console.log(data);
      });
    }
  }
  changeMode(value) {
    this.modeAuto = value;
    const mode = value ? 1 : 0;
    this.deviceService.changeMode(mode).subscribe((data) => {
      console.log(data);
    });
  }
  getPower(value) {}
}
