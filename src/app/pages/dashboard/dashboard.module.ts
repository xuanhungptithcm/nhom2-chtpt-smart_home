import { ViewsListZoneComponent } from "./views-list-zone/views-list-zone.component";
import { NgModule } from "@angular/core";
import {
  NbActionsModule,
  NbButtonModule,
  NbCardModule,
  NbTabsetModule,
  NbUserModule,
  NbRadioModule,
  NbSelectModule,
  NbListModule,
  NbIconModule,
  NbToggleModule,
} from "@nebular/theme";
import { NgxEchartsModule } from "ngx-echarts";

import { ThemeModule } from "../../@theme/theme.module";
import { DashboardComponent } from "./dashboard.component";
import { StatusCardComponent } from "./status-card/status-card.component";
import { ContactsComponent } from "./contacts/contacts.component";
import { RoomsComponent } from "./rooms/rooms.component";
import { RoomSelectorComponent } from "./rooms/room-selector/room-selector.component";
import { TemperatureComponent } from "./temperature/temperature.component";
import { TemperatureDraggerComponent } from "./temperature/temperature-dragger/temperature-dragger.component";
import { KittenComponent } from "./kitten/kitten.component";
import { SecurityCamerasComponent } from "./security-cameras/security-cameras.component";
import { ElectricityComponent } from "./electricity/electricity.component";
import { ElectricityChartComponent } from "./electricity/electricity-chart/electricity-chart.component";
import { WeatherComponent } from "./weather/weather.component";
import { SolarComponent } from "./solar/solar.component";
import { PlayerComponent } from "./rooms/player/player.component";
import { TrafficComponent } from "./traffic/traffic.component";
import { TrafficChartComponent } from "./traffic/traffic-chart.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { EarningCardBackComponent } from "./earningcard/back-side/earning-card-back.component";
import { EarningCardFrontComponent } from "./earningcard/front-side/earning-card-front.component";
import { EarningcardComponent } from "./earningcard/earningcard.component";
import { EarningLiveUpdateChartComponent } from "./earningcard/front-side/earning-live-update-chart.component";
import { EarningPieChartComponent } from "./earningcard/back-side/earning-pie-chart.component";
import { TemperatureDraggerComponent2 } from "./temperature-dragger/temperature-dragger.component";

@NgModule({
  imports: [
    FormsModule,
    ThemeModule,
    NbCardModule,
    NbUserModule,
    NbButtonModule,
    NbTabsetModule,
    NbActionsModule,
    NbRadioModule,
    NbSelectModule,
    NbListModule,
    NbIconModule,
    NbButtonModule,
    NgxEchartsModule,
    NbToggleModule,
    ReactiveFormsModule,
  ],
  declarations: [
    DashboardComponent,
    StatusCardComponent,
    TemperatureDraggerComponent,
    ContactsComponent,
    RoomSelectorComponent,
    TemperatureComponent,
    RoomsComponent,
    KittenComponent,
    SecurityCamerasComponent,
    ElectricityComponent,
    ElectricityChartComponent,
    WeatherComponent,
    PlayerComponent,
    SolarComponent,
    TrafficComponent,
    TrafficChartComponent,
    EarningCardBackComponent,
    EarningCardFrontComponent,
    EarningcardComponent,
    EarningLiveUpdateChartComponent,
    EarningPieChartComponent,
    ViewsListZoneComponent,
    TemperatureDraggerComponent2,
  ],
})
export class DashboardModule {}
