import { Component, Input, OnInit, ChangeDetectorRef } from '@angular/core';
import { SocketService } from '../../../@core/services/Socket.service';

@Component({
  selector: 'ngx-status-card',
  styleUrls: ['./status-card.component.scss'],
  template: `
    <nb-card
      style="flex-wrap: wrap;"
      (click)="light()"
      [ngClass]="{ off: !on }"
    >
      <nb-card-header style="width: 100%;">Small card</nb-card-header>
      <nb-card-body style="display: flex; align-items: center;">
        <div class="icon-container">
          <div class="icon status-{{ type }}">
            <ng-content></ng-content>
          </div>
        </div>

        <div class="details">
          <div class="title h5">{{ title }}</div>
          <div class="status paragraph-2">{{ on ? 'ON' : 'OFF' }}</div>
        </div>
      </nb-card-body>
    </nb-card>
  `
})
export class StatusCardComponent implements OnInit {
  @Input() title: string;
  @Input() type: string;
  @Input() on = true;
  @Input() numberDevice: number;
  @Input() name: string;
  @Input() nameClass: string;
  @Input() subClass: string;
  constructor(private socketService: SocketService, private cd: ChangeDetectorRef) {}
  light() {
    console.log(this.numberDevice);

    this.on = !this.on;
    if (this.on) {
      this.batDen();
    } else {
      this.tatDen();
    }
  }
  batDen() {
    this.socketService.sendMessage(this.name, {
      status: true
    });
  }
  tatDen() {
    this.socketService.sendMessage(this.name, {
      status: false
    });
  }
  ngOnInit() {
    this.socketService.getMessages('light').subscribe((data: any) => {
      console.log(data);
      if (data['den'] === this.numberDevice) {
        if (data['status'] === 'false') {
          this.on = false;
        } else {
          this.on = true;
        }
      }
      this.cd.detectChanges();
    });
   
    this.socketService
      .getMessages('login-thanh-cong')
      .subscribe((data: any) => {
        console.log(data);
        // this.messages.push(message);
        const nameDevice = data['home'];
        const arrayDevice = Object.keys(nameDevice);
        console.log(this.nameClass, this.subClass, this.numberDevice);
        if (nameDevice[this.nameClass]) {
          if (!!this.subClass) {
            if (nameDevice[this.nameClass][this.subClass]) {
              this.on = true;
            } else {
              this.on = false;
            }
          }
        }
        this.cd.detectChanges();
      });
  }
}
