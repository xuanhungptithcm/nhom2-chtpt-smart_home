/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { EarningcardComponent } from './earningcard.component';

describe('EarningcardComponent', () => {
  let component: EarningcardComponent;
  let fixture: ComponentFixture<EarningcardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EarningcardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EarningcardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
