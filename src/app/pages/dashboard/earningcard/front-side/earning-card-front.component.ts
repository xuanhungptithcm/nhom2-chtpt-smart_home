import {
  Component,
  Input,
  OnDestroy,
  OnInit,
  ChangeDetectorRef,
} from "@angular/core";
import { NbThemeService } from "@nebular/theme";
import { interval, Subscription } from "rxjs";
import { switchMap, takeWhile } from "rxjs/operators";
import { LiveUpdateChart, EarningData } from "../../../../@core/data/earning";
import { SocketService } from "../../../../@core/services/Socket.service";
import { DeviceService } from "../../../../@core/services/device.service";

@Component({
  selector: "ngx-earning-card-front",
  styleUrls: ["./earning-card-front.component.scss"],
  templateUrl: "./earning-card-front.component.html",
})
export class EarningCardFrontComponent implements OnDestroy, OnInit {
  private alive = true;
  private currentDate: Date = new Date();
  private ONE_DAY = 24 * 3600 * 1000;

  @Input() selectedCurrency: string = "nhietdo";

  intervalSubscription: Subscription;
  currencies: string[] = ["nhietdo", "doam"];
  currentTheme: string;
  earningLiveUpdateCardData: LiveUpdateChart;
  liveUpdateChartData: { value: [string, number] }[] = [];

  constructor(
    private themeService: NbThemeService,
    private socketService: SocketService,
    private earningService: EarningData,
    private cd: ChangeDetectorRef,
    private deviceService: DeviceService
  ) {
    this.themeService
      .getJsTheme()
      .pipe(takeWhile(() => this.alive))
      .subscribe((theme) => {
        this.currentTheme = theme.name;
      });
  }

  ngOnInit() {
    this.getEarningCardData(this.selectedCurrency);
    this.currentDate = this.earningService.currentDate;
    this.socketService.sendMessage("device", { typeDevice: "ui" });
    // this.socketService.getMessages("realTimeData").subscribe((data) => {
    //   console.log(data);
    // });
    this.socketService.getMessages("realTimeData").subscribe((data: any) => {
      console.log(data);
      this.currentDate = new Date(+this.currentDate + this.ONE_DAY);
      if (this.selectedCurrency.toLocaleLowerCase() === "nhietdo") {
        this.earningLiveUpdateCardData["delta"] = {
          value: data["temp"] || 30,
          up:
            data["temp"] >=
            this.liveUpdateChartData[this.liveUpdateChartData.length - 1][
              "value"
            ][1]
              ? true
              : false,
        };
        this.earningLiveUpdateCardData["name"] = "nhietdo";
        this.liveUpdateChartData.shift();
        this.liveUpdateChartData.push({
          value: [
            [
              this.currentDate.getFullYear(),
              this.currentDate.getMonth(),
              this.currentDate.getDate(),
            ].join("/"),
            data["temp"],
          ],
        });
      } else {
        this.earningLiveUpdateCardData["delta"] = {
          value: data["humidity"] || 80,
          up:
            data["humidity"] >=
            this.liveUpdateChartData[this.liveUpdateChartData.length - 1][
              "value"
            ][1]
              ? true
              : false,
        };
        this.earningLiveUpdateCardData["name"] = "doam";

        this.liveUpdateChartData.shift();
        this.liveUpdateChartData.push({
          value: [
            [
              this.currentDate.getFullYear(),
              this.currentDate.getMonth(),
              this.currentDate.getDate(),
            ].join("/"),
            data["humidity"],
          ],
        });
      }
      this.liveUpdateChartData = [...this.liveUpdateChartData];
      this.cd.detectChanges();
    });
  }

  changeCurrency(currency) {
    this.currentDate = this.earningService.currentDate;
    if (this.selectedCurrency !== currency) {
      this.selectedCurrency = currency;
      this.getEarningCardData(this.selectedCurrency);
    }
  }

  private getEarningCardData(currency) {
    this.earningService
      .getEarningCardData(currency)
      .pipe(takeWhile(() => this.alive))
      .subscribe((earningLiveUpdateCardData: LiveUpdateChart) => {
        this.earningLiveUpdateCardData = earningLiveUpdateCardData;
        this.liveUpdateChartData = earningLiveUpdateCardData.liveChart;
        this.cd.detectChanges();
      });
    this.deviceService
      .getTempWithTime(Date.now() - 1000 * 100 * 60, Date.now())
      .subscribe((data) => {
        console.log(data);
        const list: Array<any> = data["temp"];
        list.forEach((temp) => {
          this.currentDate = new Date(+this.currentDate + this.ONE_DAY);
          if (this.selectedCurrency.toLocaleLowerCase() === "nhietdo") {
            this.earningLiveUpdateCardData["delta"] = {
              value: temp["temp"] || 30,
              up:
                temp["temp"] >=
                this.liveUpdateChartData[this.liveUpdateChartData.length - 1][
                  "value"
                ][1]
                  ? true
                  : false,
            };
            this.earningLiveUpdateCardData["name"] = "nhietdo";
            this.liveUpdateChartData.shift();
            this.liveUpdateChartData.push({
              value: [
                [
                  this.currentDate.getFullYear(),
                  this.currentDate.getMonth(),
                  this.currentDate.getDate(),
                ].join("/"),
                temp["temp"],
              ],
            });
          } else {
            this.earningLiveUpdateCardData["delta"] = {
              value: temp["humidity"] || 80,
              up:
                temp["humidity"] >=
                this.liveUpdateChartData[this.liveUpdateChartData.length - 1][
                  "value"
                ][1]
                  ? true
                  : false,
            };
            this.earningLiveUpdateCardData["name"] = "doam";

            this.liveUpdateChartData.shift();
            this.liveUpdateChartData.push({
              value: [
                [
                  this.currentDate.getFullYear(),
                  this.currentDate.getMonth(),
                  this.currentDate.getDate(),
                ].join("/"),
                temp["humidity"],
              ],
            });
          }
        });
        this.liveUpdateChartData = [...this.liveUpdateChartData];
        this.cd.detectChanges();
      });
  }

  ngOnDestroy() {
    this.alive = false;
  }
}
