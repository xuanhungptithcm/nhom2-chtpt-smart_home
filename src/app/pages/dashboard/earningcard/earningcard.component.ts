import { Component, OnInit } from "@angular/core";

@Component({
  selector: "ngx-earningcard",
  templateUrl: "./earningcard.component.html",
  styleUrls: ["./earningcard.component.scss"],
})
export class EarningcardComponent implements OnInit {
  flipped = false;

  constructor() {}

  ngOnInit() {}

  toggleView() {
    this.flipped = !this.flipped;
  }
}
