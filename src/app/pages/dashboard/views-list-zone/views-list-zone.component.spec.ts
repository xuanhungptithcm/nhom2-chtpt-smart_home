import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewsListZoneComponent } from './views-list-zone.component';

describe('ViewsListZoneComponent', () => {
  let component: ViewsListZoneComponent;
  let fixture: ComponentFixture<ViewsListZoneComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewsListZoneComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewsListZoneComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
