import { Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";

import { MENU_ITEMS } from "./pages-menu";

@Component({
  selector: "ngx-pages",
  styleUrls: ["pages.component.scss"],
  template: `
    <ngx-one-column-layout>
      <nb-menu [items]="menu"></nb-menu>
      <router-outlet></router-outlet>
    </ngx-one-column-layout>
  `,
})
export class PagesComponent implements OnInit {
  constructor(private router: Router) {}
  menu = MENU_ITEMS;
  ngOnInit() {
    if (!localStorage.getItem("token")) {
      this.router.navigateByUrl("/auth/login");
    }
  }
}
