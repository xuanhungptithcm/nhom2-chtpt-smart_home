import { Component, OnInit, AfterViewInit, OnChanges } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  AbstractControl,
} from "@angular/forms";
import { Router } from "@angular/router";
import { NbToastrService, NbComponentStatus } from "@nebular/theme";
import { AuthenService } from "../../@core/services/authen.service";

@Component({
  selector: "ngx-app-login",
  templateUrl: "./login.component.html",
  styleUrls: ["./login.component.scss"],
})
export class LoginComponent implements OnInit, AfterViewInit, OnChanges {
  loginForm: FormGroup;
  rememberMe: boolean;
  submitted = false;
  result: string = "";
  constructor(
    private fb: FormBuilder,
    private authService: AuthenService,
    private router: Router,
    private toastrService: NbToastrService
  ) {
    this.loginForm = this.fb.group({
      email: ["", [Validators.required, forbiddenEmail]],
      password: [
        "",
        [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(50),
        ],
      ],
    });
  }
  ngOnChanges() {}
  ngOnInit() {
    if (!!localStorage.getItem("token")) {
      this.router.navigateByUrl("/pages/iot-dashboard");
    }
  }
  ngAfterViewInit() {}
  login() {
    if (this.loginForm.valid && !this.submitted) {
      this.submitted = true;
      this.authService
        .login(this.loginForm.value.email, this.loginForm.value.password)
        .subscribe((result) => {
          this.submitted = false;
          if (result["token"]) {
            localStorage.setItem("token", result["token"]);
            this.router.navigateByUrl("/pages/iot-dashboard");
          } else {
            this.makeToast("danger", "ERROR", result["message"]);
          }
        });
    }
  }
  showErrorMessage(message: any) {}
  makeToast(type, title, message) {
    this.showToast(type, title, message);
  }

  private showToast(type: NbComponentStatus, title: string, message: string) {
    const config = {
      status: type,
    };
    this.toastrService.show(message, title, config);
  }

  getError(element: string, name: string) {
    return this.loginForm.get(element).getError(name);
  }
}
export function forbiddenEmail(c: AbstractControl) {
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const regex = re.test(String(c.value).toLowerCase());
  return !regex
    ? {
        invalidEmail: true,
      }
    : null;
}
