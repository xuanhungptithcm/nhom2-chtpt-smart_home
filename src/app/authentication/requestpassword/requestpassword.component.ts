import { Component, OnInit, OnChanges } from "@angular/core";
import {
  FormBuilder,
  FormGroup,
  Validators,
  AbstractControl,
} from "@angular/forms";
import { Router } from "@angular/router";
import { NbToastrService, NbComponentStatus } from "@nebular/theme";
import { from, Observable } from "rxjs";
import { AuthenService } from "../../@core/services/authen.service";
import { forbiddenEmail } from "../login/login.component";

@Component({
  selector: "app-requestpassword",
  templateUrl: "./requestpassword.component.html",
  styleUrls: ["./requestpassword.component.scss"],
})
export class RequestpasswordComponent implements OnInit, OnChanges {
  submitted: boolean;
  forgotForm: FormGroup;
  constructor(
    private authService: AuthenService,
    private fb: FormBuilder,
    private router: Router,
    private toastrService: NbToastrService
  ) {}
  ngOnChanges() {}
  ngOnInit() {
    this.forgotForm = this.fb.group({
      email: ["", [Validators.required, forbiddenEmail]],
    });
  }

  requestPass() {
    if (this.forgotForm.valid && !this.submitted) {
      const email = this.forgotForm.get("email").value;
      this.submitted = true;
      this.authService.forgotPassword(email).subscribe((result) => {
        this.submitted = false;
        if (!!result["message"]) {
          this.makeToast("danger", "ERROR", result["message"]);
        } else {
          this.router.navigate(["/auth/login"]);
          this.forgotForm.get("email").setValue("");
          this.makeToast("success", "SUCCESS", "Request password success");
        }
      });
    }
  }

  makeToast(type, title, message) {
    this.showToast(type, title, message);
  }

  private showToast(type: NbComponentStatus, title: string, message: string) {
    const config = {
      status: type,
    };
    this.toastrService.show(message, title, config);
  }
  getError(element: string, name: string) {
    return this.forgotForm.get(element).getError(name);
  }
  get f() {
    return this.forgotForm.controls;
  }
}
