import { Component, OnInit } from "@angular/core";
import {
  AbstractControl,
  FormGroup,
  FormBuilder,
  Validators,
} from "@angular/forms";
import { Router } from "@angular/router";
import { NbToastrService, NbComponentStatus } from "@nebular/theme";

import { interval, Observable, of } from "rxjs";
import { take, switchMap, map } from "rxjs/operators";
import { AuthenService } from "../../@core/services/authen.service";

export function forbiddenEmail(c: AbstractControl) {
  // tslint:disable-next-line: max-line-length
  const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  const regex = re.test(String(c.value).toLowerCase());
  return !regex
    ? {
        invalidEmail: true,
      }
    : null;
}

@Component({
  // tslint:disable-next-line: component-selector
  selector: "app-active",
  templateUrl: "./active.component.html",
  styleUrls: ["./active.component.scss"],
})
export class ActiveComponent implements OnInit {
  index = 1;
  activeForm: FormGroup;
  submitted: boolean;
  emailActive: string;
  waitActive: boolean;

  constructor(
    private fb: FormBuilder,
    private authenService: AuthenService,
    private router: Router,
    private toastrService: NbToastrService
  ) {}

  ngOnInit() {
    this.emailActive = localStorage.getItem("emailActive") || "";
    this.activeForm = this.fb.group({
      email: [
        {
          value: this.emailActive,
          disabled: !!this.emailActive ? true : false,
        },
        [Validators.required, forbiddenEmail],
      ],
      code: [
        "",
        [Validators.required, Validators.minLength(6), Validators.maxLength(8)],
      ],
    });
  }

  active() {
    if (this.activeForm.valid && !this.submitted) {
      this.submitted = true;
      const email = this.emailActive || this.activeForm.get("email").value;
      const code = this.activeForm.get("code").value;
      this.authenService.active(email, code).subscribe((result) => {
        this.submitted = false;
        if (!!result["message"]) {
          this.makeToast("danger", "ERROR", result["message"]);
        } else {
          this.activeForm.reset();
          localStorage.removeItem("emailActive");
          this.router.navigateByUrl("/auth/login");
          this.makeToast("success", "SUCCESS", "Active account success");
        }
      });
    }
  }

  makeToast(type, title, message) {
    this.showToast(type, title, message);
  }

  private showToast(type: NbComponentStatus, title: string, message: string) {
    const config = {
      status: type,
    };
    this.toastrService.show(message, title, config);
  }

  getError(element: string, name: string) {
    return this.activeForm.get(element).getError(name);
  }
  get f() {
    return this.activeForm.controls;
  }
}
