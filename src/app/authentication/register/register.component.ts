import { Router } from "@angular/router";
import { Subscription, Observable, Subject } from "rxjs";
import { Component, OnInit, OnDestroy, OnChanges } from "@angular/core";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { NbToastrService, NbComponentStatus } from "@nebular/theme";

import { takeUntil } from "rxjs/operators";
import { AuthenService } from "../../@core/services/authen.service";
import { forbiddenEmail } from "../login/login.component";

// ---------- KIEM TRA LAI ------------

@Component({
  selector: "app-register",
  templateUrl: "./register.component.html",
  styleUrls: ["./register.component.scss"],
})
export class RegisterComponent implements OnInit, OnDestroy, OnChanges {
  registForm: FormGroup;
  rememberMe: boolean;
  submitted = false;
  isActive: Boolean = true;
  index = 1;
  private destroy$: Subject<void> = new Subject<void>();

  constructor(
    private authService: AuthenService,
    private fb: FormBuilder,
    private toastrService: NbToastrService,
    private router: Router
  ) {}
  ngOnChanges() {}
  ngOnInit() {
    this.registForm = this.fb.group({
      username: ["", [Validators.required, Validators.maxLength(50)]],
      email: ["", [Validators.required, forbiddenEmail]],
      password: [
        "",
        [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(50),
        ],
      ],
      confirm: [
        "",
        [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(50),
        ],
      ],
    });
  }

  register() {
    const email: string = this.registForm.get("email").value;
    const formdata = {
      username: this.registForm.get("username").value,
      email: email.trim().toLocaleLowerCase(),
      password: this.registForm.get("password").value,
    };
    if (this.registForm.valid && !this.submitted) {
      this.submitted = true;
      this.authService
        .register(formdata.email, formdata.password, formdata.username)
        .pipe(takeUntil(this.destroy$))
        .subscribe((result) => {
          this.submitted = false;
          if (!!result["message"]) {
            this.makeToast("danger", "ERROR", result["message"]);
          } else {
            this.router.navigateByUrl("/auth/active");
            localStorage.setItem("emailActive", formdata.email);
            this.registForm.reset();
            this.makeToast("success", "SUCCESS", "Register success");
          }
        });
    }
  }

  makeToast(type, title, message) {
    this.showToast(type, title, message);
  }

  private showToast(type: NbComponentStatus, title: string, message: string) {
    const config = {
      status: type,
    };
    this.toastrService.show(message, title, config);
  }

  getError(element: string, name: string) {
    return this.registForm.get(element).getError(name);
  }
  get f() {
    return this.registForm.controls;
  }

  ngOnDestroy() {
    this.destroy$.next();
    this.destroy$.complete();
  }
}
