import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class AuthenService {
  constructor(private http: HttpClient) {}
  // private baseUrl = "http://118.69.171.113:4000/api/";
  private baseUrl = "http://192.168.1.4:4000/api/";

  login(email: string, password: string) {
    const url = this.baseUrl + "auth/login";
    return this.http.post(url, {
      email: email,
      password: password,
    });
  }
  register(email: string, password: string, username: string) {
    const url = this.baseUrl + "user/register";

    return this.http.post(url, {
      email: email,
      password: password,
      username: username,
    });
  }
  active(email: string, code: string) {
    const url = this.baseUrl + "auth/active";
    return this.http.post(url, {
      email: email,
      code: code,
    });
  }
  forgotPassword(email: string) {
    const url = this.baseUrl + "auth/forgotPassword";
    return this.http.post(url, {
      email: email,
    });
  }
}
