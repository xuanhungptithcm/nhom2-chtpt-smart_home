import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

@Injectable({
  providedIn: "root",
})
export class DeviceService {
  private baseUrl = "http://192.168.1.4:4000/api/";
  // private baseUrl = "http://118.69.171.113:4000/api/";
  constructor(private http: HttpClient) {}

  getDevice() {
    const url = this.baseUrl + "device/getDevice";
    return this.http.get(url);
  }
  turnOffDevice(deviceId: string) {
    const url = this.baseUrl + "device/toggleDevice";
    return this.http.post(url, { deviceId: deviceId, check: true });
  }
  turnOnnDevice(deviceId: string) {
    const url = this.baseUrl + "device/toggleDevice";
    return this.http.post(url, { deviceId: deviceId, check: false });
  }
  getTempNow() {
    const url = this.baseUrl + "device/getTempNow";
    return this.http.get(url);
  }
  getHumiNow() {
    const url = this.baseUrl + "device/getHumiNow";
    return this.http.get(url);
  }
  getTempWithTime(start: number, end: number) {
    const url = this.baseUrl + "device/getTempWithTime";
    return this.http.post(url, { start: start, end: end });
  }
  setTemp(value: number) {
    const url = this.baseUrl + "device/setTemp";
    return this.http.post(url, { temp: value });
  }
  setHumi(value: number) {
    const url = this.baseUrl + "device/setHumi";
    return this.http.post(url, { humi: value });
  }
  changeMode(mode: number) {
    const url = this.baseUrl + "device/changeMode";
    return this.http.post(url, { mode: mode });
  }
}
