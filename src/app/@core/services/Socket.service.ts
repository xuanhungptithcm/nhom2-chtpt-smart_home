import { Injectable } from "@angular/core";
import { Socket } from "ngx-socket-io";

import { Observable } from "rxjs";

@Injectable({
  providedIn: "root",
})
export class SocketService {
  constructor(private socket: Socket) {}
  public sendMessage(name, message) {
    this.socket.emit(name, message);
  }

  getMessages(name): Observable<any> {
    return this.socket.fromEvent<any>(name);
  }
}
