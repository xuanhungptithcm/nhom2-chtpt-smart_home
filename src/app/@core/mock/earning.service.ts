import { Injectable } from "@angular/core";
import { of as observableOf, Observable } from "rxjs";
import { LiveUpdateChart, PieChart, EarningData } from "../data/earning";

@Injectable()
export class EarningService extends EarningData {
  currentDate: Date = new Date();
  private currentValue = Math.random() * 1000;
  private ONE_DAY = 24 * 3600 * 1000;
  private currentCurrency = "";
  private pieChartData = [
    {
      value: 30,
      name: "Nhiệt độ",
    },
    {
      value: 88,
      name: "Độ ẩm",
    },
  ];

  private liveUpdateChartData = {
    nhietdo: {
      liveChart: [],
      delta: {
        up: true,
        value: 30,
      },
      dailyIncome: 45895,
    },
    doam: {
      liveChart: [],
      delta: {
        up: false,
        value: 80,
      },
      dailyIncome: 5862,
    },
  };

  getDefaultLiveChartData(elementsNumber: number) {
    this.currentDate = new Date();
    this.currentValue = 0;
    return Array.from(Array(elementsNumber)).map((item) =>
      this.generateRandomLiveChartData()
    );
  }

  generateRandomLiveChartData() {
    this.currentDate = new Date(+this.currentDate + this.ONE_DAY);
    if (this.currentCurrency.toLocaleLowerCase() === "nhietdo") {
      this.currentValue = Math.random() * 5 + 30;
    } else {
      this.currentValue = Math.random() * 5 + 80;
    }

    return {
      value: [
        [
          this.currentDate.getFullYear(),
          this.currentDate.getMonth(),
          this.currentDate.getDate(),
        ].join("/"),
        Math.round(this.currentValue),
      ],
    };
  }

  getEarningLiveUpdateCardData(currency): Observable<any[]> {
    const data = this.liveUpdateChartData[currency.toLowerCase()];
    const newValue = this.generateRandomLiveChartData();
    data.liveChart.shift();
    data.liveChart.push(newValue);
    return observableOf(data.liveChart);
  }

  getEarningCardData(currency: string): Observable<LiveUpdateChart> {
    const data = this.liveUpdateChartData[currency.toLowerCase()];
    this.currentCurrency = currency;
    data.liveChart = this.getDefaultLiveChartData(10);
    return observableOf(data);
  }

  getEarningPieChartData(): Observable<PieChart[]> {
    return observableOf(this.pieChartData);
  }
}
