import {
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
  HttpResponse,
  HttpEvent,
  HttpHeaders,
} from "@angular/common/http";
import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { map, finalize } from "rxjs/operators";
import { NbToastrService, NbComponentStatus } from "@nebular/theme";

@Injectable({
  providedIn: "root",
})
export class AuthInterceptors {
  constructor() {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    req = req.clone({
      headers: this.add_Token_Header(),
    });
    return next.handle(req);
  }
  add_Token_Header() {
    const token = localStorage.getItem("token") || "";
    const headers = new HttpHeaders({
      "Content-Type": "application/json",
      Authorization: token,
    });
    return headers;
  }
}
